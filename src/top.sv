module top(input clk_25mhz, output ftdi_rxd);
    reg rst = 1;
    always @(posedge clk_25mhz) begin
        rst <= 0;
    end

    wire clk_locked;
    wire [3:0] clocks;
    wire clk_main = clocks[0];

    ecp5pll
    #(
        .in_hz(25000000),
        .out0_hz(50000000),
        .out1_hz(25000000)
    )
    ecp5pll_inst
    (
        .clk_i(clk_25mhz),
        .clk_o(clocks),
        .locked(clk_locked)
    );

    e_proj_main_main main
        ( .clk_i(clk_main)
        , .rst_i(rst)
        , .output__(ftdi_rxd)
        );
endmodule
