use camino::Utf8PathBuf;
use clap::Parser;
use color_eyre::{Result, eyre::Context};
use nesty::{code, Code};

#[derive(Parser)]
struct Args {
    #[clap()]
    indir: Utf8PathBuf,
    #[clap(long)]
    out_build_dir: Utf8PathBuf,
    #[clap(long)]
    out_spade_dir: Utf8PathBuf,
    #[clap(short='w')]
    rom_width: usize,
}

fn spade_head(day: &str, rom_width: usize) -> String {
    format!("#[no_mangle] pipeline(1) {day}_input(clk: clock, idx: int<{rom_width}>) -> int<8> __builtin__")
}

fn verilog_impl(day: &str, rom_width: usize, hex_filename: &Utf8PathBuf, infile_size: usize) -> String {
    code! {
        [0] format!("module e_{day}_input(");
        [2] "input clk_i,";
        [2] format!("input[{}:0] idx_i,", rom_width - 1);
        [2] format!("output reg[7:0] output__");
        [1] ");";
        [1]     format!("logic[7:0] data[{infile_size}-1:0];");
        [1]     "initial begin";
        [2]         format!("$readmemh(\"{hex_filename}\", data);");
        [1]     "end";
        [1]     "always @(posedge clk_i) begin";
        [1]     "   output__ <= data[idx_i];";
        [1]     "end";
        [0] "endmodule";
    }.to_string()
}

fn ramh_file(data: &[u8]) -> String {
    data.iter().map(|b| format!("{b:02x}\n")).collect()
}

fn main() -> Result<()> {
    let Args{
        indir,
        rom_width,
        out_build_dir,
        out_spade_dir,
    } = Args::parse();

    std::fs::write(out_build_dir.join("aoc_token"), "token")?;

    std::fs::create_dir_all(&out_build_dir)?;
    std::fs::create_dir_all(&out_spade_dir)?;

    for path in std::fs::read_dir(indir).unwrap() {
        let infile = path.unwrap().path();

        let day = infile.file_stem().unwrap().to_string_lossy();
        let out_verilog = out_build_dir.join(format!("{day}.sv"));
        let out_ramh = out_build_dir.join(format!("{day}.hex"));
        let out_spade = out_spade_dir.join(format!("{day}.spade"));

        println!("Dumping to {}", out_spade);

        let infile_content = std::fs::read(&infile)
            .with_context(|| format!("Failed to read {}", infile.to_string_lossy()))?;

        std::fs::write(&out_verilog, verilog_impl(&day, rom_width, &out_ramh, infile_content.len()))
            .with_context(|| format!("Failed to write {out_verilog}"))?;

        std::fs::write(&out_spade, spade_head(&day, rom_width))
            .with_context(|| format!("Failed to write {out_spade}"))?;

        std::fs::write(&out_ramh, ramh_file(&infile_content))
            .with_context(|| format!("Failed to write {out_verilog}"))?;
    }



    Ok(())
}
