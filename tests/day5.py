# top=day5::day5

from spade import *

from cocotb.clock import Clock
from cocotb.triggers import ClockCycles

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i;
    await cocotb.start(Clock(clk, 1, units="ns").start())

    s.i.rst = "true"
    # Give the memory the chance to initialize by waiting 2 cycles
    await ClockCycles(clk, 2, rising = False)
    s.i.rst = "false"

    await ClockCycles(clk, 10000, rising = False)

